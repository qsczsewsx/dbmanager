module gits.tcbs.com.vn/qe/db-manager

go 1.12

require (
	cloud.google.com/go v0.37.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190313032549-041949b8d268
	github.com/lib/pq v1.0.0
	github.com/magiconair/properties v1.8.0
	github.com/mattn/go-oci8 v0.0.0-20190205102930-2100fbe14d69
	github.com/pkg/errors v0.8.0
	github.com/spf13/viper v1.3.2
	gopkg.in/src-d/go-git.v4 v4.10.0
)
