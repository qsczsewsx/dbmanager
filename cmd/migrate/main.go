package main

import (
	"flag"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-oci8"
	"github.com/spf13/viper"
	"gits.tcbs.com.vn/qe/db-manager/util"
	. "gopkg.in/src-d/go-git.v4/_examples"
	"log"
	"os"
)

func main() {
	var cfgFile string
	var logFile string
	var listLog bool

	runCommand := flag.NewFlagSet("run", flag.ExitOnError)
	runCommand.StringVar(&cfgFile, "c", "", "path to your config file")

	rollbackCommand := flag.NewFlagSet("rollback", flag.ExitOnError)
	rollbackCommand.StringVar(&cfgFile, "c", "", "path to your config file")
	rollbackCommand.StringVar(&logFile,"f","","path to log update file")
	rollbackCommand.BoolVar(&listLog,"l",false, "list all log update file")

	if len(os.Args) == 1 {
		fmt.Println("usage: migrate <command> [<option>]")
		fmt.Println("Two command is: ")
		fmt.Println(" run   run update db to newest version")
		fmt.Println(" rollback  rollback db to specified version")
		os.Exit(1)
	} else {

		// set env cho oracle
		_ = os.Setenv("NLS_LANG", "AMERICAN_AMERICA.UTF8")

		switch os.Args[1] {
			case "run":
				err := runCommand.Parse(os.Args[2:])
				CheckIfError(err)
				if cfgFile == "" {
					log.Fatal("config file is required")
				}
			case "rollback":
				err := rollbackCommand.Parse(os.Args[2:])
				CheckIfError(err)
				if listLog == true {
					result := util.GetOldVersionFile()
					fmt.Println("List all history old version:")
					for _,filePath := range result {
						if filePath == "logs" {continue}
						fmt.Println(filePath)
					}
					os.Exit(1)
				}
				if cfgFile == "" {
					log.Fatal("config file is required")
				}
				if logFile == "" {
					log.Fatal("log update file is required")
				}
			default:
				fmt.Println("usage: migrate <command> [<option>]")
				fmt.Println("Two command is: ")
				fmt.Println(" run   run update db to newest version")
				fmt.Println(" rollback  rollback db to specified version")
				os.Exit(1)
		}

		// đọc file config bằng viper
		viper.SetConfigFile(cfgFile)
		err := viper.ReadInConfig()
		CheckIfError(err)

		// clone hoặc pull script từ git về máy
		var g util.GitInfo
		g.Repository = viper.GetString("git.url")
		g.BranchName = viper.GetString("git.branch-name")
		g.Username = viper.GetString("git.username")
		g.Password = viper.GetString("git.password")
		g.Path = viper.GetString("git.path")
		//fmt.Println(g)
		err = g.GetLatestChangesData()
		CheckIfError(err)

		//lấy list file script trong thư mục git
		listScript, err := util.GetAllScriptFile(g.Path)
		//fmt.Println(listScript)
		CheckIfError(err)

		listDbInfo := make(map[string]*util.DatabaseInfo)
		//kết nối đến DB tcbond - mssql
		tcbondInfo := &util.DatabaseInfo{}
		tcbondInfo.Name = "tcbond"
		tcbondInfo.Type = viper.GetString(tcbondInfo.Name + ".type")
		tcbondInfo.Host = viper.GetString(tcbondInfo.Name + ".host")
		tcbondInfo.Username = viper.GetString(tcbondInfo.Name + ".username")
		tcbondInfo.Password = viper.GetString(tcbondInfo.Name + ".password")

		err = tcbondInfo.Connection()
		CheckIfError(err)
		listDbInfo[tcbondInfo.Name] = tcbondInfo

		//kết nối đến db tcinvest - mssql
		var tcinvestInfo= &util.DatabaseInfo{}
		tcinvestInfo.Name = "tcinvest"
		tcinvestInfo.Type = viper.GetString(tcinvestInfo.Name + ".type")
		tcinvestInfo.Host = viper.GetString(tcinvestInfo.Name + ".host")
		tcinvestInfo.Username = viper.GetString(tcinvestInfo.Name + ".username")
		tcinvestInfo.Password = viper.GetString(tcinvestInfo.Name + ".password")
		err = tcinvestInfo.Connection()
		CheckIfError(err)
		listDbInfo[tcinvestInfo.Name] = tcinvestInfo

		//kết nối đến db cas - oracle
		var casInfo= &util.DatabaseInfo{}
		casInfo.Name = "cas"
		casInfo.Type = viper.GetString(casInfo.Name + ".type")
		casInfo.Host = viper.GetString(casInfo.Name + ".host")
		casInfo.Username = viper.GetString(casInfo.Name + ".username")
		casInfo.Password = viper.GetString(casInfo.Name + ".password")
		err = casInfo.Connection()
		CheckIfError(err)
		listDbInfo[casInfo.Name] = casInfo

		var otpInfo= &util.DatabaseInfo{}
		otpInfo.Name = "otp"
		otpInfo.Type = viper.GetString(otpInfo.Name + ".type")
		otpInfo.Host = viper.GetString(otpInfo.Name + ".host")
		otpInfo.Username = viper.GetString(otpInfo.Name + ".username")
		otpInfo.Password = viper.GetString(otpInfo.Name + ".password")
		err = otpInfo.Connection()
		CheckIfError(err)
		listDbInfo[otpInfo.Name] = otpInfo
		//đóng các connection
		defer util.CloseAllConnection(listDbInfo)

		switch os.Args[1] {
			case "run":
				//tự lấy last version và chạy script
				util.RunAllScript(listDbInfo, listScript)
			case "rollback":
				var oldVersionScript = make(map[string]string)
				v := viper.New()
				v.SetConfigFile(logFile)
				err := v.ReadInConfig()
				CheckIfError(err)
				oldVersionScript["tcbond"] = v.GetString("tcbond")
				oldVersionScript["tcinvest"] = v.GetString("tcinvest")
				oldVersionScript["cas"] = v.GetString("cas")
				oldVersionScript["otp"] = v.GetString("otp")
				//rollback ve version trong file logFile
				util.RollbackScript(listDbInfo,listScript,oldVersionScript)
			default:
				log.Fatal("command is invalid")
		}
	}
}
