#Database migrate

##Install

Cài Go theo hướng dẫn https://golang.org/doc/install

Sau đó chạy lệnh
````
    go get gitlab.com/qsczsewsx/dbmanager/cmd/migrate
````

##Run
````
	migrate run -c D:\QEtool\db-manager\config.properties
````
##Rollback
````
	migrate rollback -c D:\QEtool\db-manager\config.properties -f fileOldVersionPath
````
Để xem list fileOldversion dùng lệnh
````
    migrate rollback -l
````
##Help
````
    migrate run --help
    migrate rollback --help
````
##Example
Xem ví dụ config.properties [tại đây](config.properties)

 Các loại DB đang hỗ trợ:
 
 mssql: microsoft sql server
 
 oracle: oracle db
 
 postgres: postgres db
 
...