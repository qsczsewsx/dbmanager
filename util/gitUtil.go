package util

import (
	"fmt"
	"gopkg.in/src-d/go-git.v4"
	. "gopkg.in/src-d/go-git.v4/_examples"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"os"
)

type GitInfo struct {
	Repository string
	BranchName string
	Username string
	Password string
	Path string
	w git.Worktree
}

func (g *GitInfo) gitClone() error {
	url := fmt.Sprintf("https://%s:%s@%s", g.Username, g.Password, g.Repository)
	Info("git clone --branch %s %s",g.BranchName,url)
	r, err := git.PlainClone(g.Path, false, &git.CloneOptions{
		URL:        url,
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", g.BranchName)),
		Progress:   os.Stdout,
	})
	if r != nil {fmt.Println(r)}
	return  err
}

func (g *GitInfo) setWorktree() error {
	r, err := git.PlainOpen(g.Path)
	if err != nil {
		return err
	}

	// Get the working directory for the repository
	w, err := r.Worktree()
	if err != nil {
		return err
	}

	g.w = *w
	return err
}

func (g *GitInfo) gitPull() error {
	err := g.setWorktree()
	if err != nil {
		return err
	}
	Info("git pull origin")
	err = g.w.Pull(&git.PullOptions{
		RemoteName:    "origin",
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", g.BranchName)),
		Progress:      os.Stdout,
		Force:         true,
	})
	return err
}

func (g *GitInfo) gitCheckout() error {
	err := g.setWorktree()
	if err != nil {
		return err
	}
	Info("git checkout %s", g.BranchName)
	err = g.w.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", g.BranchName)),
		Force:  true,
	})
	return err
}
func (g *GitInfo)GetLatestChangesData() error {
	// clone tcbsdb from git
	err := g.gitClone()

	// if repository exist then pull new data
	if err != nil {
		if err.Error() == "repository already exists" {
			// pull new data
			err = g.gitPull()

			// if pull false, try with git checkout
			if err != nil {
				err = g.gitCheckout()
			}
		}
	}
	return err
}
