package util

import (
	"fmt"
	"time"
)

func GetTimeFormatYyyymmddHHiiss() string {
	result := ""
	t := time.Now()
	result = fmt.Sprintf("%d%02d%02d_%02d%02d%02d",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	return result
}