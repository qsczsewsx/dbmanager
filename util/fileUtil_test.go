package util

import (
	. "gopkg.in/src-d/go-git.v4/_examples"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestGetAllScriptFile(t *testing.T) {
	watchDir := filepath.Join("folderTest", "sproc")
	os.MkdirAll(watchDir, os.ModePerm)
	watchDir = filepath.Join("folderTest", "update_data")
	os.MkdirAll(watchDir, os.ModePerm)
	watchDir = filepath.Join("folderTest", "v.1.0.0")
	os.MkdirAll(watchDir, os.ModePerm)
	watchDir = filepath.Join("folderTest", "rollback")
	os.MkdirAll(watchDir, os.ModePerm)
	defer  os.RemoveAll("folderTest")
	result, err := GetAllScriptFile("folderTest")
	CheckIfError(err)
	if len(result)!= 3 {
		t.Error("expected ignore folder update_data, rollback")
	}
}

func TestWriteOldVersionFile(t *testing.T) {
	defer os.RemoveAll("logs")
	var m = make(map[string]string)
	m["ahihi"]="yolooo"
	filePath := WriteOldVersionFile(m)
	r,err := ioutil.ReadFile(filePath)
	CheckIfError(err)
	if string(r) != "ahihi = yolooo\n" {
		t.Error("expected data in file is: ahihi = yolooo\n")
	}
	if !strings.Contains(filePath,"old_version_") ||
		!strings.Contains(filePath,".properties") {
		t.Error("expected file name like old_version_yyyymmdd_hhiiss.properties")
	}
}
