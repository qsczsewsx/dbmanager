package util

import (
	"github.com/spf13/viper"
	. "gopkg.in/src-d/go-git.v4/_examples"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func GetAllScriptFile(path string) ([]string,error) {
	var listFile = make([]string,0)
	err := filepath.Walk(path,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !strings.Contains(path,".git") &&
				!strings.Contains(path,"README") &&
				!strings.Contains(path,"\\rollback") &&
				!strings.Contains(path,"\\ROLLBACK") &&
				!strings.Contains(path,"UPDATE_DATA") &&
				!strings.Contains(path,"update_data") &&
				!strings.Contains(path,"v.0.0.0") {
				listFile = append(listFile,path)
			}
			return nil
		})
	if err != nil {
		return nil, err
	}
	return listFile,nil
}

func GetOldVersionFile()[]string {
	var listFile = make([]string,0)
	watchDir := filepath.Join(".", "logs")
	os.MkdirAll(watchDir, os.ModePerm)
	listFile, err := GetAllScriptFile("logs")
	CheckIfError(err)
	return listFile
}

func WriteOldVersionFile(listOldVersion map[string]string)string{
	v := viper.New()
	for dbname,script := range listOldVersion {
		v.Set(dbname,script)
	}
	watchDir := filepath.Join(".", "logs")
	os.MkdirAll(watchDir, os.ModePerm)
	fileName := "old_version_" + GetTimeFormatYyyymmddHHiiss() + ".properties"
	configFile := path.Join(watchDir, fileName)
	//v.SetConfigFile(configFile)
	err := v.WriteConfigAs(configFile)
	CheckIfError(err)
	return configFile
}