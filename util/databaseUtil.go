package util

import (
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
	. "gopkg.in/src-d/go-git.v4/_examples"
	"io/ioutil"
	"strings"
)

type LastVersion struct {
	Version        string
	LastRunScript string
	LastRunDate   string
}

type DatabaseInfo struct {
	Name     string
	Type     string
	Host     string
	Username string
	Password string
	con      *sql.DB
}

func (d *DatabaseInfo) Connection() error {
	var connection *sql.DB
	var err error
	if d.Type == "mssql" {
		connectString := fmt.Sprintf("server=%s;user id=%s;password=%s;", d.Host, d.Username, d.Password)
		connection, err = sql.Open("mssql", connectString)
		CheckIfError(err)
		err = connection.Ping()
		CheckIfError(err)
	} else if d.Type == "oracle" {
		connectString := fmt.Sprintf("%s/%s@%s", d.Username, d.Password, d.Host)
		//fmt.Println(casConnectString)
		connection, err = sql.Open("oci8", connectString)
		CheckIfError(err)
		err = connection.Ping()
		CheckIfError(err)
	} else if d.Type == "postgres" {
		connectString := fmt.Sprintf("postgres://%s:%s@%s?sslmode=disable",
			d.Username, d.Password, d.Host)
		connection, err = sql.Open("postgres", connectString)
		CheckIfError(err)
		err = connection.Ping()
		CheckIfError(err)
	} else {
		return errors.New(fmt.Sprintf("database type %s not yet supported", d.Type))
	}
	d.con = connection
	return err
}

func (d *DatabaseInfo) excuteQuery(query string) (*sql.Rows, error) {
	return d.con.Query(query)
}

func runScript(m map[string]*DatabaseInfo, scriptFile string) (*sql.Rows, error) {
	fmt.Println("Start run script file: ",scriptFile)
	dat, err := ioutil.ReadFile(scriptFile)
	CheckIfError(err)
	pathScript := strings.Split(scriptFile, "\\")
	DatabaseName := strings.Split(pathScript[2], ".")[1]
	result, err := m[DatabaseName].excuteQuery(string(dat))
	//defer result.Close()
	if err == nil {
		fmt.Println("==>successful")
	} else {
		fmt.Println("==> ERROR: ",err.Error())
	}
	return result, err
}

func (d *DatabaseInfo)runRollback( scriptFile string) (*sql.Rows, error) {
	dat, err := ioutil.ReadFile(scriptFile)
	CheckIfError(err)
	pathScript := strings.Split(scriptFile, "\\")
	DatabaseName := strings.Split(pathScript[3], ".")[1]
	if DatabaseName == d.Name {
		fmt.Println("Start run script file: ",scriptFile)
		result, err := d.excuteQuery(string(dat))
		//defer result.Close()
		if err == nil {
			fmt.Println("==>successful")
		} else {
			fmt.Println("==> ERROR: ", err.Error())
		}
		return result, err
	} else {
		fmt.Println(d.Name," : ",scriptFile,"=>not run")
		return nil,nil
	}
}

func GetLastVersionRunScript(m map[string]*DatabaseInfo) (string, map[string]string, bool) {
	listLastVersionRunScript := make(map[string]string)
	lastVersionRunScript := ""
	var version sql.NullString
	var lastRunScript sql.NullString
	metadataSync := true
	prefixCode := ""
	query := "select version, last_run_script from %smetadata where id=1"
	//lấy version và last_run_script trong tất cả các DB và add vào listLastVersionRunScript
	for dbname, dbInf := range m {
		switch dbInf.Type {
		case "mssql":
			prefixCode = "[" + dbInf.Username + "].dbo."
		default:
			prefixCode = ""
		}
		result, err := dbInf.excuteQuery(fmt.Sprintf(query, prefixCode))
		// tạo table metadata và insert default data nếu chưa có
		if err != nil {
			fmt.Println("get version on DB ",dbInf.Name, " error: ",err.Error())
			CreateMetadataTable(dbInf)
			result, err = dbInf.excuteQuery(fmt.Sprintf(query, prefixCode))
		}
		CheckIfError(err)
		if result.Next() {
			err = result.Scan(&version, &lastRunScript)
			//CheckIfError(err)
			lastRunScriptOnThisDB := "tcbsdb\\" + version.String + "\\" + lastRunScript.String
			listLastVersionRunScript[dbname] = lastRunScriptOnThisDB
			fmt.Println("last version and run script on", dbname, "is:", lastRunScriptOnThisDB)
			//nếu version trong DB này > lastVersionRunScript thì update lastVersionRunScript
			if lastRunScriptOnThisDB != lastVersionRunScript {
				if lastVersionRunScript != "" {
					metadataSync = false
				}
				if lastRunScriptOnThisDB > lastVersionRunScript {
					lastVersionRunScript = lastRunScriptOnThisDB
				}
			}
		}
		result.Close()
	}

	fmt.Println("last version and run script is: ", lastVersionRunScript)
	return lastVersionRunScript, listLastVersionRunScript, metadataSync
}

func (d *DatabaseInfo) syncMetadata(listScript []string, lastVersionRunScriptOnDB string, lastVersionRunScript string) {
	fmt.Println("version on databases not compare => start sync")
	var version string
	var lastRunScript string
	for _, script := range listScript {
		pathScript := strings.Split(script, "\\")
		if len(pathScript) >=3 && pathScript[2]!="" {
			version = pathScript[1]
			lastRunScript = pathScript[2]
			dbnameOnFile := strings.Split(pathScript[2], ".")[1]
			if script <= lastVersionRunScriptOnDB {
				continue
			} else if script > lastVersionRunScript {
				break
			} else {
				if dbnameOnFile == d.Name {
					dat, err := ioutil.ReadFile(script)
					CheckIfError(err)
					result, err := d.excuteQuery(string(dat))
					if result != nil {result.Close()}
					CheckIfError(err)
				}
				d.UpdateMetadata(version, lastRunScript)
			}
		}
	}
}

func (d *DatabaseInfo) UpdateMetadata(version string, lastRunScript string) {
	prefixCode := ""
	switch d.Type {
	case "mssql":
		prefixCode = "[" + d.Username + "].dbo."
	default:
		prefixCode = ""
	}
	query := fmt.Sprintf("update %smetadata set version='%s', last_run_script='%s', last_run_date=default where id=1",
		prefixCode, version, lastRunScript)
	result, err := d.excuteQuery(query)
	defer result.Close()
	CheckIfError(err)
}

func UpdateAllMetadata(m map[string]*DatabaseInfo, version string, lastRunScript string) {
	for _, dbInf := range m {
		dbInf.UpdateMetadata(version, lastRunScript)
	}
}

func RunAllScript(m map[string]*DatabaseInfo, listScript []string) {
	// lấy last version và script từ DB
	lastVersionRunScript, listLastVersionRunScript, flagSync := GetLastVersionRunScript(m)
	// lưu lại version của các db trước khi chạy
	WriteOldVersionFile(listLastVersionRunScript)
	//nếu version trên các DB không giống nhau thì đồng bộ lại
	if !flagSync {
		for dbname, lastVersionRunScriptOnDB := range listLastVersionRunScript {
			if lastVersionRunScriptOnDB < lastVersionRunScript {
				m[dbname].syncMetadata(listScript, lastVersionRunScriptOnDB, lastVersionRunScript)
			}
		}
	}
	listSproc := make([]string,0)
	for _, script := range listScript {
		pathScript := strings.Split(script, "\\")
		if len(pathScript) >=3 && pathScript[2]!="" {
			version := pathScript[1]
			lastRunScript := pathScript[2]
			if version == "SPROC" || version == "sproc" {
				listSproc = append(listSproc, script)
			} else if script > lastVersionRunScript {
				result, err := runScript(m, script)
				if result != nil {result.Close()}
				CheckIfError(err)
				UpdateAllMetadata(m, version, lastRunScript)
			}
		}
	}

	//run all script for sproc
	reloadSproc(m,listSproc)
}

func reloadSproc(m map[string]*DatabaseInfo, listScript []string) {
	for _, script := range listScript {
		fmt.Println("Run SPROC file: ",script)
		result, err := runScript(m, script)
		if result != nil {result.Close()}
		CheckIfError(err)
	}
}

func CreateMetadataTable(dbInfo *DatabaseInfo) {
	fmt.Println("create table metadata on DB ",dbInfo.Name)
	createQuery, err := ioutil.ReadFile("tcbsdb\\v.0.0.0\\"+dbInfo.Name+".create-table-metadata.sql")
	CheckIfError(err)
	result,err := dbInfo.excuteQuery(string(createQuery))
	if result != nil {result.Close()}
	CheckIfError(err)
	fmt.Println("insert default data to table metadata on DB ",dbInfo.Name)
	insertQuery, err := ioutil.ReadFile("tcbsdb\\v.0.0.0\\"+dbInfo.Name+".insert-data-default-metadata.sql")
	CheckIfError(err)
	result,err = dbInfo.excuteQuery(string(insertQuery))
	if result != nil {result.Close()}
	CheckIfError(err)
}

func RollbackScript(m map[string]*DatabaseInfo,listScript []string, oldVersionScript map[string]string){
	// lấy last version và script từ DB
	lastVersionRunScript, listLastVersionRunScript, _ := GetLastVersionRunScript(m)

	for i:=len(listScript)-1;i>=0;i-- {
		script := listScript[i]
		if script > lastVersionRunScript {
			continue
		} else {
			numOfRun := 0
			pathScript := strings.Split(script, "\\")
			if len(pathScript) >=3 && pathScript[2]!="" {
				version := pathScript[1]
				lastRunScript := pathScript[2]
				rollbackScript := pathScript[0] +"\\"+version+"\\rollback\\"+lastRunScript
				//dbname := strings.Split(lastRunScript,".")[1]
				if version == "SPROC" || version == "sproc" {
					continue
				} else {
					for dn,dbInfo := range m {
						if script > oldVersionScript[dn] &&
							script <= listLastVersionRunScript[dn] {
							numOfRun++
							result, err := dbInfo.runRollback(rollbackScript)
							if result != nil {result.Close()}
							CheckIfError(err)
							for true {
								if i > 0 {
									prevScript := listScript[i-1]
									prevPathScript := strings.Split(prevScript, "\\")
									if len(prevPathScript) >= 3 && prevPathScript[2]!="" {
										prevVersion := prevPathScript[1]
										prevLastRunScript := prevPathScript[2]
										if prevVersion == "SPROC" || prevVersion == "sproc" {
											i--
											continue
										}
										dbInfo.UpdateMetadata(prevVersion, prevLastRunScript)
										break
									} else {
										i--
										continue
									}
								} else {
									dbInfo.UpdateMetadata("v.0.0.0", "")
									break
								}
							}
						}
					}
					if numOfRun == 0 {break}
				}
			}
		}
	}
}

func CloseAllConnection(m map[string]*DatabaseInfo) error {
	for _, dbInfo := range m {
		err := dbInfo.con.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
